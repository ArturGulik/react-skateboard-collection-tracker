import 'react';
import logo from '/skateboard.svg';
import './style/Navbar.css';

export default function Navbar() {
    return (
      <div className="navbar">
        <img id="page-logo" src={logo} />
        <span id="page-title">Skateboard Collection Tracker</span>
      </div>
    );
  }
  