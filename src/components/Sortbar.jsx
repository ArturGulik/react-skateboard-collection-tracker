import 'react';
import './style/Sortbar.css';

export default function Sortbar(props) {
    return (
      <div className="sortbar">
        <label htmlFor="add-button">Add:</label>
        <button name="add-button" className="add-button" onClick={props.onAddElement}>
          +
        </button>
        <label htmlFor="sortby">Sort by:</label>
        <select name="sortby" id="sortby" value={props.sortBy}
        onChange={(event) => props.onSortChange(event.target.value)}>
          <option value="id">Date added</option>
          <option value="name">Name</option>
          <option value="rating">Rating</option>
          <option value="width">Deck width</option>
        </select>

        <label htmlFor="search">Search:</label>
        <input type="text"
          value={props.searchWord}
          onChange={(event) => props.onSearch(event.target.value)}
        ></input>
      </div>
    );
  }
  