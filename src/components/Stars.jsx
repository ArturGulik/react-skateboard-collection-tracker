import 'react';
import './style/Stars.css';

function createMultiple(n, what) {
  return Array.from({length: n}, (_, index) => {
    return what(index);
  });
}

export default function Stars(props) {
    let max = props.maxAmount ?? 5;
    let amount = props.amount ?? 0;

    return (
      <div className="stars-container">
        {createMultiple(amount, (index) => (
        <button className="star" key={index} onClick={() => props.onStarChange(index+1)}>
          <span className="star-span">★</span>
          </button>
        ))}
        {createMultiple(max-amount, (index) => (
        <button className="star" key={index} onClick={() => props.onStarChange(amount+1+index)}>
          <span className="star-span">☆</span>
          </button>
        ))}
      </div>
    );
  }
  