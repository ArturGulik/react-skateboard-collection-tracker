import { useState } from 'react';
import logo from '/skateboard.svg';
import './style/NewElementDialog.css';
import Stars from './Stars';
import Skateboard from '../Skateboard';

function flash(elementId) {
  let elem = document.getElementById(elementId);
  elem.addEventListener("animationend", () => {
    elem.classList.remove("flash");
  });
  elem.classList.add("flash");
}
export default function NewElementDialog(props) {
  const [starAmount, setStarAmount] = useState(5);
  const [title, setTitle] = useState("");
  const [imageurl, setImageurl] = useState("");
  const [skateboardWidth, setSkateboardWidth] = useState("");
  const [skateboardMaterial, setSkateboardMaterial] = useState("");
  const [skateboardType, setSkateboardType] = useState("");
  const [description, setDescription] = useState("");

  document.body.style.overflow = "hidden";

  let closeDialog = () => {
    let screenFilter = document.getElementById("screen-darkening-div");
    screenFilter.classList.add("removing");
    let animations = 2;
    screenFilter.addEventListener("animationend", () => {
      animations--;
      if (!animations) {
        document.body.style.overflow = "unset";
        props.onCancel();
      }
    });
  };

  let onSubmit = () => {
    let errors = false;
    if (title === "") {
      flash("title-input");
      errors = true;
    }
    if (imageurl === "") {
      flash("imageurl-input");
      errors = true;
    }
    if (skateboardWidth === "") {
      flash("width-input");
      errors = true;
    }
    if (skateboardType === "") {
      flash("type-input");
      errors = true;
    }
    if (skateboardMaterial === "") {
      flash("material-input");
      errors = true;
    }
    if (description === "") {
      flash("description-input");
      errors = true;
    }
    if (errors) return;

    let skateboard = new Skateboard({
      name: title,
      imgsrc: imageurl,
      description: description,
      width: skateboardWidth,
      deckType: skateboardMaterial,
      rating: starAmount,
      type: skateboardType
    });

    closeDialog();
    props.onSubmit(skateboard);
  };

  return (
    <div id="screen-darkening-div" className="screen-darkening">
      <div className="new-element-dialog">
        <span id="new-element-title">Add a skateboard</span>
        <button className="close-dialog-button" onClick={closeDialog}>×</button>
        <div className="input-groups first-group">
          <div className="input-group">
            <label htmlFor="title">Skateboard name: </label>
            <input name="title" id="title-input" placeholder="New Skateboard" type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            ></input>
            <label htmlFor="title">Image URL: </label>
            <input name="imageurl" id="imageurl-input" placeholder="https://example.com/sb.jpg" type="text"
              value={imageurl}
              onChange={(e) => setImageurl(e.target.value)}
            ></input>
          </div>
          <div className="input-group">
            <label htmlFor="title">Skateboard width: </label>
            <input name="width" id="width-input" placeholder='8"' type="text"
              value={skateboardWidth}
              onChange={(e) => setSkateboardWidth(e.target.value)}
            ></input>
            <label htmlFor="material">Skateboard material: </label>
            <input name="material" id="material-input" placeholder='7-ply maple' type="text"
              value={skateboardMaterial}
              onChange={(e) => setSkateboardMaterial(e.target.value)}
            ></input>
          </div>
        </div>
        <div className="input-groups">
          <div className="input-group">
            <label htmlFor="type">Skateboard type: </label>
            <input name="type" id="type-input" placeholder='Park Skateboard' type="text"
              value={skateboardType}
              onChange={(e) => setSkateboardType(e.target.value)}
            ></input>
          </div>
          <div className="input-group">
            <label>Rating:</label>
            <Stars amount={starAmount} onStarChange={setStarAmount}></Stars>
          </div>
        </div>
        <label htmlFor="title">Description: </label>
        <textarea name="width" id="description-input" placeholder="Board description" type="text"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        ></textarea>
        <button className="submit-dialog-button" onClick={onSubmit}>Submit</button>
      </div>
    </div>
  );
}
  