import SkateboardComp from './SkateboardComp.jsx';
import './style/Collection.css';

export default function Collection(props) {
    return (
        <div className="collection">{
            props.collection.map((skateboard, index) => {
                return (
                <SkateboardComp
                    skateboard={skateboard}
                    key={skateboard.id}
                    onStarChange={props.onStarChange(skateboard.id)}
                    removeSkateboard={props.removeSkateboard}
                />)
            })
        }</div>
    );
}