import 'react';
import './style/SkateboardComp.css';
import Stars from './Stars';
import trashLidImg from '/trash-lid.png';
import trashCanImg from '/trash-can.png';
import parkBoard from '/park-board.png';
import longboard from '/longboard.png';
import pennyboard from '/pennyboard.png';
import casterboard from '/casterboard.png';

// An object used to ensure proper image loading for GitLab Pages
let images = {
  "/casterboard.png": casterboard,
  "/pennyboard.png": pennyboard,
  "/longboard.png": longboard,
  "/park-board.png": parkBoard
}

const maxDescLength = 180;

function cropText(text, length) {
    if (text.length <= length)
        return text;
    return text.slice(0, length-3) + "...";
}

export default function SkateboardComp(props) {
    return (
      <div className="skateboard-list-element" id={"sle"+props.skateboard.id}>
        <div className="thumbnail-container" id={"st"+props.skateboard.id}
            onMouseOver={(event) => {
              let e = document.getElementById("st"+props.skateboard.id);
              e.classList.remove("mouse-out");
              e.classList.add("mouse-over");
            }}
            onMouseOut={() => {
              let e = document.getElementById("st"+props.skateboard.id);
              e.classList.remove("mouse-over");
              e.classList.add("mouse-out");
            }}
        >
        <img className="skateboard-thumbnail" src={images[props.skateboard.imgsrc] ?? props.skateboard.imgsrc} />
        <button className="remove-button" onClick={ () => {
          let e = document.getElementById("st"+props.skateboard.id);
          e.classList.add("removing");
          let sle = document.getElementById("sle"+props.skateboard.id);
            sle.classList.add("removing");
          setTimeout(() => {
            props.removeSkateboard(props.skateboard.id);
          }, 1800);
        }
        }>
          <img className="remove-image-top" src={trashLidImg} />
          <img className="remove-image-bottom" src={trashCanImg} />
        </button>
        </div>
        <div className="skateboard-info-panel">
        <span className="skateboard-name">{props.skateboard.name}</span>
        <div className="skateboard-details">
            <span>{props.skateboard.width}</span>
            •
            <span>{props.skateboard.deckType}</span>
            •
            <span>{props.skateboard.type}</span>
            •
            <Stars amount={props.skateboard.rating} onStarChange={props.onStarChange}/>
        </div>
        <div className="skateboard-description">{props.skateboard.description}</div>
        </div>
      </div>
    )
  }
  