import { useEffect, useState } from 'react';
import './App.css';
import Skateboard from './Skateboard';
import Navbar from './components/Navbar.jsx';
import Sortbar from './components/Sortbar.jsx';
import Collection from './components/Collection.jsx';
import skateboardsFromDb from './SkateboardsDb.jsx';
import projectPath from './projectPath.jsx';
import NewElementDialog from './components/NewElementDialog.jsx';

function sortByKey(key, ascending=true) {
  return (a, b) => {
    if (a[key] < b[key])
      return (ascending)? -1 : 1;
    if (a[key] > b[key])
      return (ascending)? 1 : -1;
    return 0;
  }
}

export default function App() {
  const [collection, setCollection] = useState([]);
  const [sortBy, _setSortField] = useState("id");
  const [searchWord, _setSearchWord] = useState("");
  const [addingNewElement, setAddingNewElement] = useState(false);

  let setSortField = (newSortField) => {
    _setSortField(newSortField);
    setCollection((prev) => {
      return prev.toSorted(sortByKey(newSortField, newSortField!=="rating"));
    });
  };

  let setSearchWord = (newSearchWord) => {
    _setSearchWord(newSearchWord.toLowerCase());
  };

  let onStarChange = (skateboardId) => {
    return (newRating) => {
      setCollection((oldCollection) => {
        let newCollection = oldCollection.map(sb => {
          if (sb.id === skateboardId)
            sb.rating = newRating;
          return sb;
        });
        if (sortBy === "rating")
          newCollection.sort(sortByKey("rating", false))
        return newCollection;
      });
    }
  }

  let removeSkateboard = (skateboardId) => {
    setCollection((oldCollection) => {
      return oldCollection.filter((sb) => sb.id !== skateboardId);
    })
  }

  let toggleAddDialog = () => {
    setAddingNewElement((prev) => !prev);
  };

  let addSkateboard = (skateboard) => {
    setCollection((prev) => {
      return [...prev, skateboard];
    });
  };

  useEffect(() => {
    fetch(projectPath + "/skateboards.json")
    .then(res => res.json())
    .then(json => {
      setCollection(
        json.skateboards.map((args) => {
          return new Skateboard(args);
        })
      )},
      (_) => {
        setCollection(
          skateboardsFromDb.map((args) => {
            return new Skateboard(args);
          })
        );
      })
  }, []);

  return (
    <>
      {(addingNewElement)?
        <NewElementDialog onCancel={toggleAddDialog}
          onSubmit={addSkateboard}
        ></NewElementDialog>
       : <></>}
      <Navbar />
      <Sortbar
        onAddElement={toggleAddDialog}
        sortBy={sortBy} onSortChange={setSortField}
        searchWord={searchWord} onSearch={setSearchWord}
      />
      <Collection onStarChange={onStarChange}
        collection={collection.filter((skateboard) => skateboard.containsString(searchWord))}
        removeSkateboard={removeSkateboard}
      />
    </>
  )
}
