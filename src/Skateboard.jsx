let idGenerator = ((generator) => {
  return () => generator.next().value
})((function* () {
  let i=0;
  while(true) {
    yield i++;
  }
})());

export default class Skateboard {
  constructor(args) {
    this.name = args.name ?? "";
    this.wheels = args.wheels ?? "";
    this.imgsrc = args.imgsrc ?? "";
    this.description = args.description ?? "";
    this.width = args.width ?? "";
    this.type = args.type ?? "";
    this.deckType = args.deckType ?? "";
    this.rating = args.rating ?? 1;
    this.id = idGenerator();
  }

  toString() {
    return this.name + this.wheels + this.imgsrc
    + this.description + this.width + this.type + this.deckType + String(this.rating);
  }

  copy() {
    return new Skateboard(this);
  }

  containsString(string) {
    return this.toString().toLowerCase().includes(string);
  }
}