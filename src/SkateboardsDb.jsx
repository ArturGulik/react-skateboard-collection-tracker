/*
  This file is only used as a backup if fetching JSON data fails.
  This can happen on GitLab pages due to Cross-Origin Request policies.
*/

let skateboards = [
    {
        "name": "Bamboo Blank",
        "wheels": "104A 42mm",
        "width": "8.25\"",
        "imgsrc": "/park-board.png",
        "description": "Crafted with precision and durability, the bamboo skateboard represents a significant upgrade from its inexpensive counterparts. The bamboo's natural strength ensures longevity, withstanding the rigors of skateboarding sessions while providing an eco-friendly alternative to traditional skateboards.",
        "type": "Park Skateboard",
        "deckType": "6-ply bamboo",
        "rating": 5
    },
    {
        "name": "Maple Glide Cruiser",
        "wheels": "89A 70mm",
        "width": "9\"",
        "imgsrc": "/longboard.png",
        "description": "Cruising just got a whole lot smoother with this 7-ply maple longboard! With its sturdy yet flexible deck, this board offers a buttery-smooth ride, absorbing bumps and cracks effortlessly. Whether you're commuting or just chilling with friends, this longboard is all about relaxed vibes and endless fun!",
        "type": "Longboard",
        "deckType": "7-ply maple",
        "rating": 4
    },
    {
        "name": "Penny Bolt Cruiser",
        "wheels": "89A 70mm",
        "width": "7\"",
        "imgsrc": "/pennyboard.png",
        "description": "My penny board is a compact, vibrant orange cruiser with smooth wheels and a sturdy deck. It's my go-to for cruising around town, zipping through streets and weaving between pedestrians with ease. Its small size makes it perfect for quick trips and spontaneous adventures.",
        "type": "Penny Board",
        "deckType": "plastic",
        "rating": 2
    },
    {
        "name": "Elite Shadow Swivel",
        "wheels": "95A 48mm",
        "width": "6.25\"",
        "imgsrc": "/casterboard.png",
        "description": "My black caster board is a sleek and agile ride with swiveling decks and high-performance wheels. It's designed for quick turns and smooth glides, making it perfect for navigating busy streets or carving up the pavement in the park. Its compact size and lightweight construction make it easy to carry anywhere for an exciting ride on the go.",
        "type": "Caster Board",
        "deckType": "plastic",
        "rating": 3
    }
];

export default skateboards;