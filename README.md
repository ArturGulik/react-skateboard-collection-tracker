[![pipeline](https://gitlab.com/ArturGulik/react-skateboard-collection-tracker/badges/main/pipeline.svg)](https://gitlab.com/ArturGulik/react-skateboard-collection-tracker/-/commits/main)

# React Skateboard Collection Tracker

A React Demo built using Vite. Presents an interactive list of skateboards with cool animations.

## See for yourself

The demo is hosted online.
### [`View demo`](https://arturgulik.gitlab.io/react-skateboard-collection-tracker/)

## Tinkering guide

Clone the repository and run a `npm install` in it.

Once all dependencies are installed you can use scripts defined for this project:
- `npm run build` - to build the application into a static website (creates the `dist` directory with the resulting website)
- `npm run dev` - to build the project, serve it on localhost and run watch scripts that update the `dist` directory's contents as well as the page content in real time, whenever a source file changes